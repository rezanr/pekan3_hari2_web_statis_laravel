<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Latihan Hari 1</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="welcome" method="post">
		@csrf
		<label>First Name :</label><br>
		<input type="text" name="name" placeholder="insert your first name" required>
		<br><br>
		<label>Last Name :</label><br>
		<input type="text" name="fullname" placeholder="insert your last name">
		<br><br>
		<label>Gender</label><br>
		<input type="radio" name="gender"><label>Male</label><br>
		<input type="radio" name="gender"><label>Female</label><br>
		<input type="radio" name="gender"><label>Other</label><br><br>
		<label>Nationality</label><br>
		<select name="nationality" id="nation" required>
			<option></option>
			<option value="american">American</option>
			<option value="aborigin">Aborigin</option>
			<option value="european">European</option>
			<option value="indonesian">Indonesia</option>
			<option value="malaysian">Malaysian</option>
		</select><br><br>
		<label>Language Spoken :</label><br>
		<input type="checkbox"><label>Bahasa Indonesia</label><br>
		<input type="checkbox"><label>English</label><br>
		<input type="checkbox"><label>Other</label><br><br>
		<label>Bio :</label><br><br>
		<textarea name="Bio" id="" cols="35" rows="10" required></textarea><br>
		<input type="submit" value="Sign Up">
		</form>

</body>
</html>
